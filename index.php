<html>
	<head>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.css">
81044
81045
81046
81047
81048
81049
81050
	</head>
</html>
<body>
	<div class="grid-container grid-x grid-padding-x align-center">
		<div class="small-8">
			<h1>Artikelnummern öffnen im Shop</h1>
			<p>Im folgenden Textfeld können pro Zeile Artikelnummern reinkopiert werden, die dann automatisiert in einem neuen Tab geöffnet werden. Dabei ist es egal ob es eine 5-stellige oder gar 7-stellige Artikelnummer aus dem Angela Bruderer Shop ist. Es ist sogar möglich unterschiedliche Suchbegriffe einzutippen.<br>
			<strong>Wichtig:</strong> Ein Artikel oder Suchbegriff pro Zeile.</p>
			<form action="<?= basename() ?>" method="post">
				<textarea name="artikelnummern" id="artikelnummern" cols="30" rows="10"><?php echo $_POST['artikelnummern'] ?></textarea>
				<input class="button float-right" type="submit" name="Öffnen" value="Artikel öffnen">
			</form>
		</div>
	</div>
	<?php $articles = explode("\n", $_POST['artikelnummern']); ?>
	<div class="grid-container grid-x grid-padding-x align-center">
		<div class="small-8">
			<?php if (!empty($articles)) { ?>
				<p>Sollte mal ein Link nicht öffnen, können in der untenstehenden Liste auch alle aufgerufenen Seiten nochmals geöffnet werden.</p>
				<?php
				foreach ($articles as $article) { ?>
					<script>
						window.open('https://www.angela-bruderer.ch/praesenz/search?suchtext=<?= trim($article) ?>', '_blank');
					</script>
					<?php 
					linkHandler(trim($article));
				}
			} else { ?>
				<div class="callout warning">
					<h5>Keine Übereinstimmungen gefunden.</h5>
					<p>Bitte gib eine Artikelnummer oder einen Suchbegriff ein.</p>
				</div>
			<?php } ?>
		</div>
	</div>
</body>
<?php
function linkHandler($keyword) {
	if ($keyword != "") {
		if (preg_match('/[0-9]/', $keyword)) {
			echo "Artikel: ";
		} else {
			echo "Suchbegriff: ";
		}
	}

	echo '<a class="links" target="_blank" href="https://www.angela-bruderer.ch/praesenz/search?suchtext=' . $keyword . '">' . $keyword . '</a><br>';
} ?>